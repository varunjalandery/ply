const { expect, } = require('chai');
const Validator = require('../../../src/lib/Validator');


const validData = {
    age: 19,
    monthlyIncome: 10000,
    latitude: 75.55,
    longitude: 125.45,
    experienced: 'true',
};

const inValidData = {
    age: 234,
    monthlyIncome: 'thisIsMonthlyIncome',
    latitude: 175.55,
    longitude: 2125.45,
    experienced: 'AbsolutelyTrue',
};

describe('src/lib/Validator', () => {
    it('isValidMonthlyIncome() : should return true for valid monthly income', () => {
        const validator = new Validator();
        validator.setData(validData);
        validator.check('monthlyIncome').isMonthlyIncomeValid();
        expect(validator.isOk()).to.equal(true);
    });

    it('isAgeValid() : should return true for valid age', () => {
        const validator = new Validator();
        validator.setData(validData);
        validator.check('age').isAgeValid();
        expect(validator.isOk()).to.equal(true);
    });

    it('isExperienceValid() : should return true for valid experience', () => {
        const validator = new Validator();
        validator.setData(validData);
        validator.check('experienced').isExperienceValid();
        expect(validator.isOk()).to.equal(true);
    });

    it('isLatitudeValid() : should return true for valid latitude', () => {
        const validator = new Validator();
        validator.setData(validData);
        validator.check('latitude').isLatitudeValid();
        expect(validator.isOk()).to.equal(true);
    });

    it('isLongitudeValid() : should return true for valid longitude', () => {
        const validator = new Validator();
        validator.setData(validData);
        validator.check('longitude').isLongitudeValid();
        expect(validator.isOk()).to.equal(true);
    });

    it('isValidMonthlyIncome() : should return false for invalid monthly income', () => {
        const validator = new Validator();
        validator.setData(inValidData);
        validator.check('monthlyIncome').isMonthlyIncomeValid();
        expect(validator.isOk()).to.equal(false);
    });

    it('isAgeValid() : should return false for invalid age', () => {
        const validator = new Validator();
        validator.setData(inValidData);
        validator.check('age').isAgeValid();
        expect(validator.isOk()).to.equal(false);
    });

    it('isExperienceValid() : should return false for invalid experience', () => {
        const validator = new Validator();
        validator.setData(inValidData);
        validator.check('experienced').isExperienceValid();
        expect(validator.isOk()).to.equal(false);
    });

    it('isLatitudeValid() : should return false for invalid latitude', () => {
        const validator = new Validator();
        validator.setData(inValidData);
        validator.check('latitude').isLatitudeValid();
        expect(validator.isOk()).to.equal(false);
    });

    it('isLongitudeValid() : should return true for invalid longitude', () => {
        const validator = new Validator();
        validator.setData(inValidData);
        validator.check('longitude').isLongitudeValid();
        expect(validator.isOk()).to.equal(false);
    });

    it('isNotEmpty() : should return true because longitude exists', () => {
        const validator = new Validator();
        validator.setData(inValidData);
        validator.check('longitude').isNotEmpty();
        expect(validator.isOk()).to.equal(true);
    });

    it('isNotEmpty() : should return false because apple does not exists', () => {
        const validator = new Validator();
        validator.setData(inValidData);
        validator.check('apple').isNotEmpty();
        expect(validator.isOk()).to.equal(false);
    });

    it('checkAndGetInt() : should return int for valid int', () => {
        const validator = new Validator();
        validator.setData(validData);
        expect(validator.check('age').checkAndGetInt()).to.equal(19);
    });

    it('checkAndGetInt() : should return false for invalid int', () => {
        const validator = new Validator();
        validator.setData(validData);
        expect(validator.check('experienced').checkAndGetInt()).to.equal(false);
    });

    it('checkAndGetFloat() : should return float for invalid float', () => {
        const validator = new Validator();
        validator.setData(validData);
        expect(validator.check('longitude').checkAndGetFloat()).to.equal(125.45);
    });

    it('checkAndGetFloat() : should return false for invalid float', () => {
        const validator = new Validator();
        validator.setData(validData);
        expect(validator.check('experienced').checkAndGetFloat()).to.equal(false);
    });

    it('isNumberBetween() : should return true for because 123 is between 100 and 200', () => {
        const validator = new Validator();
        validator.setData(validData);
        expect(validator.isNumberBetween(123, 100, 200)).to.equal(true);
    });

    it('isNumberBetween() : should return false for because 123 is not between 40 and 100', () => {
        const validator = new Validator();
        validator.setData(validData);
        expect(validator.isNumberBetween(123, 40, 100)).to.equal(false);
    });

    it('isOk() : should return true for correct validation', () => {
        const validator = new Validator();
        validator.setData(validData);
        validator
            .check('age').isAgeValid()
            .check('monthlyIncome').isMonthlyIncomeValid()
            .check('experienced')
            .isExperienceValid()
            .check('latitude')
            .isLatitudeValid()
            .check('longitude')
            .isLongitudeValid();

        expect(validator.isOk()).to.equal(true);
    });

    it('isNotOk() : should return true for incorrect validation', () => {
        const validator = new Validator();
        validator.setData(inValidData);
        validator
            .check('age').isAgeValid()
            .check('monthlyIncome').isMonthlyIncomeValid()
            .check('experienced')
            .isExperienceValid()
            .check('latitude')
            .isLatitudeValid()
            .check('longitude')
            .isLongitudeValid();

        expect(validator.isNotOk()).to.equal(true);
    });

    it('getErrors() : should return errors for invalid data', () => {
        const validator = new Validator();
        validator.setData(inValidData);
        validator
            .check('age').isAgeValid()
            .check('monthlyIncome').isMonthlyIncomeValid()
            .check('experienced')
            .isExperienceValid()
            .check('latitude')
            .isLatitudeValid()
            .check('longitude')
            .isLongitudeValid();
        expect(validator.getErrors()).to.include(
            {
                age: '234 is not valid age',
                monthlyIncome: 'thisIsMonthlyIncome is not valid monthly income',
                experienced: 'AbsolutelyTrue is not valid experience',
                latitude: '175.55 is not valid latitude',
                longitude: '2125.45 is not valid longitude',
            }
        );
    });
});
