const { expect, } = require('chai');
const Query = require('../../../../src/lib/solr/Query');

describe('src/lib/solr/Query', () => {
    it('getFilterQuery() : should return correct filter query', () => {
        const query = new Query('Test_Query');
        expect(query.getFilterQuery({
            filterName: 'age',
            filterValue: 21,
            boost: 30,
        })).to.equal('age:21^30');
    });

    it('getRangeFilterQuery() : should return correct range filter query', () => {
        const query = new Query('Test_Query');
        expect(query.getRangeFilterQuery({
            filterName: 'monthlyIncome',
            min: 5000,
            max: 8000,
            boost: 75,
        })).to.equal('monthlyIncome:[5000 TO 8000]^75');
    });


    it('getLocationFilterQuery() : should return correct location filter query', () => {
        const query = new Query('Test_Query');
        expect(query.getLocationFilterQuery({
            latlong: {
                lat: 75.23,
                long: 123.32,
            },
            scoringAlgo: 'recipDistance',
            locationFieldName: 'location',
            distance: 30,
            boost: 20,
        })).to.equal('{!geofilt score=recipDistance sfield=location pt=75.23,123.32 d=30 }^20');
    });

    it('bulid() : should return correct query', () => {
        const query = new Query('Test_Query');
        query.addScalarFilter('age', 35, 20)
            .addRangeFilter('monthlyIncome', 3000, 7000, 50)
            .addLocationFilter(
                { lat: 85.24, long: 165.45, },
                'location',
                20,
                50,
                'recipDistance'
            )
            .addSort()
            .addReturnFields();

        expect(query.build()).to.equal('age:35^20 AND monthlyIncome:[3000 TO 7000]^50 '
            + 'AND {!geofilt score=recipDistance sfield=location pt=85.24,165.45 d=50 }^20'
            + '&fl=*,score&sort=score desc');
    });
});
