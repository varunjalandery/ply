const { expect, } = require('chai');
const PeopleLikeYouLib = require('../../../src/lib/PeopleLikeYou');


describe('src/lib/PeopleLikeYou', () => {
    it('validateCriteria() : should return validator object', () => {
        const criteria = {
            age: 45,
            experienced: 'true',
            monthlyIncome: 12000,
            latitude: '82.15',
            longitude: '120.12',
        };
        const plyObj = new PeopleLikeYouLib(criteria);
        const validator = plyObj.validateCriteria();
        expect(validator.constructor.name).to.equal('Validator', 'returned object should be of type Validator');
    });

    it('isOk() : should return true if correct criteria is passed', () => {
        const criteria = {
            age: 45,
            experienced: 'true',
            monthlyIncome: 12000,
            latitude: '82.15',
            longitude: '120.12',
        };
        const plyObj = new PeopleLikeYouLib(criteria);
        const validator = plyObj.validateCriteria();
        expect(validator.isOk()).to.equal(true, 'isOk() should return true');
    });

    it('isOk() : should return false if incorrect criteria is passed', () => {
        const criteria = {
            age: 45,
            experienced: 'true',
            monthlyIncome: 12000,
            latitude: '82.15',
            longitude: '181.12',
        };
        const plyObj = new PeopleLikeYouLib(criteria);
        const validator = plyObj.validateCriteria();
        expect(validator.isOk()).to.equal(false, 'isOk() should return false');
    });


    it('isNotOk() : should return false if correct criteria is passed', () => {
        const criteria = {
            age: 45,
            experienced: 'true',
            monthlyIncome: 12000,
            latitude: '82.15',
            longitude: '120.12',
        };
        const plyObj = new PeopleLikeYouLib(criteria);
        const validator = plyObj.validateCriteria();
        expect(validator.isNotOk()).to.equal(false, 'isNotOk() should return false');
    });

    it('isNotOk() : should return true if incorrect criteria is passed', () => {
        const criteria = {
            age: 45,
            experienced: 'true',
            monthlyIncome: 12000,
            latitude: '82.15',
            longitude: '181.12',
        };
        const plyObj = new PeopleLikeYouLib(criteria);
        const validator = plyObj.validateCriteria();
        expect(validator.isNotOk()).to.equal(true, 'isNotOk() should return true');
    });

    it('bulidQuery() : should return object of type Query', () => {
        const criteria = {
            age: 45,
            experienced: 'true',
            monthlyIncome: 12000,
            latitude: '82.15',
            longitude: '120.12',
        };
        const plyObj = new PeopleLikeYouLib(criteria);
        expect(plyObj.buildQuery().constructor.name).to.equal('Query', 'buildQuery should return object of type Query');
    });

    it('bulidQuery() : query object should have filters applied', () => {
        const criteria = {
            age: 45,
            experienced: 'true',
            monthlyIncome: 12000,
            latitude: '82.15',
            longitude: '120.12',
        };
        const plyObj = new PeopleLikeYouLib(criteria);
        const queryObj = plyObj.buildQuery();
        expect(
            queryObj.filters.map((filter => filter.filterName))
        ).to.include.members(['age', 'experienced', 'monthlyIncome', 'location']);
    });

    it('getBoosts() : should return correct boosts', () => {
        const criteria = {
            age: 45,
            experienced: 'true',
            monthlyIncome: 12000,
            latitude: '82.15',
            longitude: '120.12',
        };
        const plyObj = new PeopleLikeYouLib(criteria);
        const boosts = plyObj.getBoosts();
        expect(boosts.monthlyIncome).to.equal(54, 'monthlyIncome boost should be 50');
        expect(boosts.age).to.equal(20, 'age boost should be 50');
        expect(boosts.location).to.equal(22, 'location boost should be 50');
    });
});
