const csv = require('fast-csv');
const async = require('async');
const config = require('../config');
const SolrCollection = require('../src/lib/solr/Collection');

class SendDocsToSolr {
    constructor() {
        this.solrCollectionInvestors = new SolrCollection('investors');
        this.queue = async.queue((task, callback) => {
            this.solrCollectionInvestors.addDocuments([task])
                .then(() => {
                    console.log('Done', task.name);
                    callback();
                })
                .catch((err) => {
                    console.error('Not Done', err, task.name);
                    callback(err);
                });
        }, 5);
    }

    execute() {
        csv
            .fromPath(config.dumpDir, { headers: true, })
            .on('data', (data) => {
                this.queue.push({
                    name: data.name,
                    age: data.age,
                    latlong: `${data.latitude},${data.longitude}`,
                    monthlyIncome: data['monthly income'],
                    experienced: data.experienced === 'true',
                });
            })
            .on('end', () => {
                console.log('Job is done.');
            });
    }
}

const sendDocsToSolr = new SendDocsToSolr();
sendDocsToSolr.execute();

module.exports = SendDocsToSolr;
