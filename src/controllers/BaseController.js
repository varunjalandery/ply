class BaseController {
    static getOptionalParams(paramsInReq, paramsToCheck) {
        const optionalParams = {};
        paramsToCheck.forEach((paramName) => {
            if (typeof paramsInReq[paramName] !== 'undefined') {
                optionalParams[paramName] = paramsInReq[paramName];
            }
        });
        if (optionalParams.latitude && !optionalParams.longitude) {
            delete optionalParams.latitude;
        }
        if (!optionalParams.latitude && optionalParams.longitude) {
            delete optionalParams.longitude;
        }
        return optionalParams;
    }
}

module.exports = BaseController;
