const BaseController = require('./BaseController');
const PeopleLikeYouLib = require('../lib/PeopleLikeYou');

class PeopleLikeYou extends BaseController {
    static index(req, res, next) {
        const validParams = PeopleLikeYou.getOptionalParams(req.query, [
            'age',
            'latitude',
            'longitude',
            'monthlyIncome',
            'experienced',
        ]);
        const peopleLikeYouLib = new PeopleLikeYouLib(validParams);
        const validator = peopleLikeYouLib.validateCriteria(validParams);
        if (validator.isNotOk()) {
            res.send({ errors: validator.getErrors(), });
        } else {
            peopleLikeYouLib.search(validParams)
                .then(data => res.send({
                    peopleLikeYou: peopleLikeYouLib.formatter(data.response.docs),
                }))
                .catch(err => res.send(500, err));
        }
        return next();
    }
}

module.exports = PeopleLikeYou;
