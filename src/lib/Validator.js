const util = require('util');

class Validator {
    constructor() {
        this.data = {};
        this.errors = {};
        this.key = null;
    }

    check(key) {
        this.key = key;
        return this;
    }

    isNotEmpty(msg = '%s should not be empty.') {
        if (typeof this.data[this.key] === 'undefined'
            || this.data[this.key] === ''
            || this.data[this.key] === null) {
            this.pushError(msg);
        }
        return this;
    }


    isMonthlyIncomeValid(msg = '%s is not valid monthly income') {
        const possibleIntValue = this.checkAndGetInt();
        if (!possibleIntValue
            || !this.isNumberBetween(possibleIntValue, 1, 1000000000)) {
            this.pushError(msg);
        }
        return this;
    }

    isAgeValid(msg = '%s is not valid age') {
        const possibleIntValue = this.checkAndGetInt();
        if (!possibleIntValue
            || !this.isNumberBetween(possibleIntValue, 1, 110)) {
            this.pushError(msg);
        }
        return this;
    }

    isLongitudeValid(msg = '%s is not valid longitude') {
        const possibleFloatValue = this.checkAndGetFloat();
        if (!possibleFloatValue
            || !this.isNumberBetween(possibleFloatValue, -180, 180)) {
            this.pushError(msg);
        }
        return this;
    }

    isLatitudeValid(msg = '%s is not valid latitude') {
        const possibleFloatValue = this.checkAndGetFloat();
        if (!possibleFloatValue
            || !this.isNumberBetween(possibleFloatValue, -90, 90)) {
            this.pushError(msg);
        }
        return this;
    }

    isExperienceValid(msg = '%s is not valid experience') {
        if (['true', 'false'].indexOf(this.getValue()) === -1) {
            this.pushError(msg);
        }
        return this;
    }

    checkAndGetInt() {
        let val = this.getValue();
        val = parseInt(val, 10);
        if (isNaN(val)) { // eslint-disable-line no-restricted-globals
            return false;
        }
        return val;
    }

    checkAndGetFloat() {
        let val = this.getValue();
        val = parseFloat(val);
        if (isNaN(val)) { // eslint-disable-line no-restricted-globals
            return false;
        }
        return val;
    }

    isNumberBetween(number, min, max) {
        if (number <= max && number >= min) {
            return true;
        }
        return false;
    }

    isOk() {
        return Object.keys(this.errors).length === 0;
    }

    isNotOk() {
        return Object.keys(this.errors).length > 0;
    }

    getErrors() {
        return this.errors;
    }


    pushError(msg) {
        this.errors[this.key] = util.format(msg, this.data[this.key]);
    }

    getValue() {
        if (typeof this.data[this.key] === 'undefined') {
            throw new Error(`Trying to get key = '${this.key}' which is undefined.`);
        }
        return `${this.data[this.key]}`;
    }

    setData(data) {
        this.data = data;
    }
}

module.exports = Validator;
