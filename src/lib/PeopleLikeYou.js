const Validator = require('./Validator');
const Collection = require('./solr/Collection');
const Query = require('./solr/Query');

class PeopleLikeYou {
    constructor(criteria) {
        this.criteria = criteria;
    }

    validateCriteria() {
        const validator = new Validator();
        validator.setData(this.criteria);
        Object.keys(this.criteria).forEach((filterName) => {
            switch (filterName) {
            case 'longitude':
                validator.check(filterName).isLongitudeValid();
                break;

            case 'latitude':
                validator.check(filterName).isLatitudeValid();
                break;

            case 'age':
                validator.check(filterName).isAgeValid();
                break;

            case 'experienced':
                validator.check(filterName).isExperienceValid();
                break;

            case 'monthlyIncome':
                validator.check(filterName).isMonthlyIncomeValid();
                break;

            default:
                console.error('Unknown People Like You Criteria', filterName);
            }
        });
        return validator;
    }

    getBoosts() {
        let totalBoost = 99;
        const boosts = {
            monthlyIncome: 0,
            location: 0,
            age: 0,
        };

        if (Object.keys(this.criteria)
            .every(criterion => ['age', 'monthlyIncome', 'longitude', 'latitude'].indexOf(criterion) === -1)
        ) {
            return boosts;
        }
        while (totalBoost > 0) {
            if (this.criteria.monthlyIncome) {
                totalBoost -= 5;
                boosts.monthlyIncome += totalBoost < 5 ? totalBoost : 5;
            }
            if (this.criteria.longitude) {
                totalBoost -= 2;
                boosts.location += totalBoost < 2 ? totalBoost : 2;
            }
            if (this.criteria.age) {
                totalBoost -= 2;
                boosts.age += totalBoost < 2 ? totalBoost : 2;
            }
        }
        return boosts;
    }

    buildQuery() {
        const query = new Query('PeopleLikeYou_Query');
        const location = {};
        const boosts = this.getBoosts();
        Object.keys(this.criteria).forEach((filterName) => {
            switch (filterName) {
            case 'longitude':
                location.long = this.criteria[filterName];
                break;

            case 'latitude':
                location.lat = this.criteria[filterName];
                break;

            case 'age':
                query.addRangeFilter(
                    'age',
                    parseInt(this.criteria[filterName], 10) - 5,
                    parseInt(this.criteria[filterName], 10) + 5,
                    boosts.age
                );
                break;

            case 'experienced':
                query.addScalarFilter('experienced', this.criteria[filterName]);
                break;

            case 'monthlyIncome':
                query.addRangeFilter(
                    'monthlyIncome',
                    Math.floor(parseInt(this.criteria[filterName], 10) * 0.9),
                    Math.ceil(parseInt(this.criteria[filterName], 10) * 1.9),
                    boosts.monthlyIncome
                );
                break;

            default:
                console.error('Unknown People Like You Criteria', filterName);
            }
        });

        if (location.lat && location.long) {
            query.addLocationFilter(
                {
                    lat: location.lat,
                    long: location.long,
                },
                'latlong',
                boosts.location,
            );
        }
        if (Object.keys(this.criteria).length === 0) {
            query.addScalarFilter('*', '*');
        }
        query
            .addSort()
            .addReturnFields();
        return query;
    }

    search() {
        const query = this.buildQuery(this.criteria);
        const investorsCollection = new Collection('investors');
        return investorsCollection.search(query.build());
    }

    formatter(docs) {
        const formattedDocs = [];
        let location = null;
        let isLocationOnly = false;
        if (this.criteria.latitude
            && this.criteria.longitude
            && Object.keys(this.criteria).length === 2) {
            isLocationOnly = true;
        }
        docs.forEach((doc) => {
            location = doc.latlong.split(',');
            formattedDocs.push({
                name: doc.name,
                age: doc.age,
                latitude: location[0],
                longitude: location[1],
                monthlyIncome: doc.monthlyIncome,
                experienced: doc.experienced,
                score: isLocationOnly ? doc.score : doc.score / 100,
            });
        });
        return formattedDocs;
    }
}

module.exports = PeopleLikeYou;
