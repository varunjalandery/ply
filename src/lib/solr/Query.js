class Query {
    constructor(name) {
        this.name = name;
        this.filters = [];
        this.sort = [];
        this.fields = [];
    }

    addSort(sort = ['score', 'desc']) {
        this.sort = sort;
        return this;
    }

    addReturnFields(fields = ['*', 'score']) {
        this.fields = fields;
        return this;
    }

    addScalarFilter(filterName, filterValue, boost = null) {
        const filter = { type: 'simple', };
        filter.filterName = filterName;
        filter.filterValue = filterValue;
        if (boost) {
            filter.boost = boost;
        }
        this.filters.push(filter);
        return this;
    }

    addRangeFilter(filterName, min, max, boost = null) {
        const filter = { type: 'range', };
        filter.filterName = filterName;
        filter.min = min;
        filter.max = max;
        if (boost) {
            filter.boost = boost;
        }
        this.filters.push(filter);
        return this;
    }

    addLocationFilter(latlong, locationFieldName, boost, distance = 50, scoringAlgo = 'recipDistance') {
        const filter = { type: 'location', filterName: 'location', };
        filter.latlong = latlong;
        filter.locationFieldName = locationFieldName;
        filter.boost = boost;
        filter.distance = distance;
        filter.scoringAlgo = scoringAlgo;
        this.filters.push(filter);
        return this;
    }

    build() {
        const filterStrings = [];
        this.filters.forEach((filter) => {
            switch (filter.type) {
            case 'simple':
                filterStrings.push(this.getFilterQuery(filter));
                break;

            case 'range':
                filterStrings.push(this.getRangeFilterQuery(filter));
                break;

            case 'location':
                filterStrings.push(this.getLocationFilterQuery(filter));
                break;

            default:
                throw new Error(`Unknown filter type ${filter.type}`);
            }
        });
        return `${filterStrings.join(' AND ')}&fl=${this.fields.join(',')}&sort=${this.sort.join(' ')}`;
    }

    getFilterQuery(filter) {
        let filterString = `${filter.filterName}:${filter.filterValue}`;
        if (filter.boost) {
            filterString += `^${filter.boost}`;
        }
        return filterString;
    }

    getRangeFilterQuery(filter) {
        let filterString = `${filter.filterName}:[${filter.min} TO ${filter.max}]`;
        if (filter.boost) {
            filterString += `^${filter.boost}`;
        }
        return filterString;
    }

    getLocationFilterQuery(filter) {
        const locString = `${filter.latlong.lat},${filter.latlong.long}`;
        return `{!geofilt score=${filter.scoringAlgo} sfield=${filter.locationFieldName} pt=${locString} d=${filter.distance} }^${filter.boost}`;
    }
}

module.exports = Query;
