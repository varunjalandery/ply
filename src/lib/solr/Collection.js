require('dotenv').config();
const request = require('request');
const config = require('../../../config');

class Collection {
    constructor(collectionName) {
        this.collectionName = collectionName;
    }

    addDocuments(documents) {
        return this.callSolr(
            {
                method: 'POST',
                url: `${this.collectionName}/update`,
                qs: { commitWithin: '1000', overwrite: 'true', wt: 'json', },
            },
            documents
        );
    }

    search(query) {
        return this.callSolr({
            method: 'GET',
            url: `${this.collectionName}/select?q=${query}`,
        });
    }

    callSolr(options = {}, body = null) {
        const opts = {
            method: 'GET',
            url: `${config.solr.protocol}://${config.solr.host}:${config.solr.port}/solr`,
            headers: {
                'Content-Type': 'application/json',
            },
            json: true,
        };
        if (options.method) {
            opts.method = options.method;
        }
        if (options.url) {
            opts.url = `${opts.url}/${options.url}`;
        }
        if (options.qs) {
            opts.qs = options.qs;
        }

        if (body) {
            opts.body = body;
        }
        return new Promise((resolve, reject) => {
            request(opts, (err, _, resBody) => {
                if (err) {
                    console.error('Solr Query Failed for collection', this.collectionName);
                    return reject(err);
                }
                return resolve(resBody);
            });
        });
    }
}

module.exports = Collection;
