module.exports = {
    solr : {
        host: process.env.SOLR_HOST,
        port : process.env.SOLR_PORT,
        protocol: process.env.SOLR_PROTOCOL
    },
    baseDir: process.cwd(),
    dumpDir: process.cwd() + '/dump/data.csv'
}