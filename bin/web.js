require('dotenv').config();
const restify = require('restify');

const PeopleLikeYouController = require('../src/controllers/PeopleLikeYou');

const server = restify.createServer();
server.pre(restify.pre.sanitizePath());
server.use(restify.plugins.queryParser());
server.get('/people-like-you', PeopleLikeYouController.index);
server.listen(8080, () => {
    console.log('%s listening at %s', server.name, server.url);
});
