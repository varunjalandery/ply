if (process.argv.length < 2) {
    console.err('Please provide a job name.');
    process.exit(0);
}
require('dotenv').config();
const { fork, } = require('child_process');

fork(`cliJobs/${process.argv[2]}.js`);
